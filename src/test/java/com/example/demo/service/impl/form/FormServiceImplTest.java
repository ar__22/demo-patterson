package com.example.demo.service.impl.form;

import com.example.demo.entity.form.FormAnswerEntity;
import com.example.demo.entity.form.FormEntity;
import com.example.demo.entity.form.answerenum.AnswerEnum;
import com.example.demo.entity.user.UserEntity;
import com.example.demo.repository.form.FormAnswerRepository;
import com.example.demo.repository.form.FormRepository;
import com.example.demo.repository.user.UserRepository;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(JMockit.class)
public class FormServiceImplTest {

    @Tested
    private FormServiceImpl sut;

    @Injectable
    private FormRepository formRepository;

    @Injectable
    private FormAnswerRepository formAnswerRepository;

    @Injectable
    private UserRepository userRepository;

    final static private Long ID = Long.valueOf(1);
    final static private String TOPIC = "DEPORTE";

    @Test
    public void createFormTestOK() throws Exception {

        final UserEntity mockedUserResponse = buildUserEntity();
        final FormEntity mockedFormResponse = buildFormEntity();

        new Expectations() {
            {
                userRepository.getOne(ID);
                times = 1;
                returns(mockedUserResponse);

                formRepository.save((FormEntity) any);
                times = 1;
                returns(mockedFormResponse);
            }
        };

        final FormEntity actual = sut.createForm(ID, TOPIC);

        assertNotNull(actual);
        assertEquals("Topic", mockedFormResponse.getTopic(), actual.getTopic());
        assertEquals("Url", mockedFormResponse.getUrl(), actual.getUrl());
    }

    @Test(expected = Exception.class)
    public void createFormTestKO() throws Exception {


        new Expectations() {
            {
                userRepository.getOne(ID);
                times = 1;
                returns(null);
            }
        };

        final FormEntity actual = sut.createForm(ID, TOPIC);
    }

    @Test
    public void saveAnswerTest() throws Exception {
        final FormAnswerEntity mockedFormAnswerEntity = buildFormAnswerEntity();

        new Expectations() {
            {
                formAnswerRepository.save((FormAnswerEntity) any);
                times = 1;
                returns(mockedFormAnswerEntity);
            }
        };

        final FormAnswerEntity actual = sut.saveAnswer(ID, AnswerEnum.REAL_MADRID.name(), TOPIC);

        assertNotNull(actual);
        assertEquals("Answer", mockedFormAnswerEntity.getAnswer(), actual.getAnswer());
        assertEquals("Topic", mockedFormAnswerEntity.getTopic(), actual.getTopic());
    }

    private UserEntity buildUserEntity() {
        final UserEntity userEntity = new UserEntity();
        userEntity.setUserName("mockedUsername");
        userEntity.setId(ID);
        return userEntity;
    }

    private FormEntity buildFormEntity() {
        final FormEntity formEntity = new FormEntity();

        formEntity.setId(ID);
        formEntity.setUserId(ID);
        formEntity.setTopic(TOPIC);
        formEntity.setUrl("url");

        return formEntity;
    }

    private FormAnswerEntity buildFormAnswerEntity() {
        final FormAnswerEntity formAnswerEntity = new FormAnswerEntity();

        formAnswerEntity.setTopic(TOPIC);
        formAnswerEntity.setFormId(ID);
        formAnswerEntity.setAnswer(AnswerEnum.REAL_MADRID.name());
        formAnswerEntity.setId(ID);

        return formAnswerEntity;
    }
}
