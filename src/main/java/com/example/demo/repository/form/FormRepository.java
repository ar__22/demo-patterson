package com.example.demo.repository.form;

import com.example.demo.entity.form.FormEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FormRepository extends JpaRepository<FormEntity, Long> {

    List<FormEntity> getByUserIdAndTopic(final Long userId, final String topic);
}
