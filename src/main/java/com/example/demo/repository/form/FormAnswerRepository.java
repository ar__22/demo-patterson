package com.example.demo.repository.form;

import com.example.demo.entity.form.FormAnswerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FormAnswerRepository extends JpaRepository<FormAnswerEntity, Long> {

    FormAnswerEntity getByFormId(final Long formId);

}
