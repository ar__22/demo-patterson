package com.example.demo.entity.form.answerenum;

public enum AnswerEnum {

    REAL_MADRID,
    LIVERPOOL,
    JUVENTUS;

    static public boolean isMember(final String answer) {

        for (final AnswerEnum value : AnswerEnum.values()) {
            if (answer.equals(value.name())) {
                return true;
            }
        }
        return false;
    }
}
