package com.example.demo.entity.form;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "form_answer_table")
@Data
public class FormAnswerEntity {

    @Id
    @GeneratedValue
    private Long id;
    private Long formId;
    private String answer;
    private String topic;
}
