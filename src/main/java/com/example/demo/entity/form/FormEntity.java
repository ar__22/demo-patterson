package com.example.demo.entity.form;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "form_table")
@Data
public class FormEntity {

    @Id
    @GeneratedValue
    private Long id;
    private Long userId;
    private String url;
    private String topic;
}
