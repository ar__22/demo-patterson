package com.example.demo.entity.dashboard;

import lombok.Data;

@Data
public class DashboardEntity {

    private String userName;
    private String topic;
    private String answer;
}
