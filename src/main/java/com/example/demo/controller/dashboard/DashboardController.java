package com.example.demo.controller.dashboard;

import com.example.demo.service.form.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo-patterson/v1")
public class DashboardController {

    @Autowired
    private FormService formService;

    @RequestMapping(value = "/getDashboardInfo", method = RequestMethod.GET)
    public ResponseEntity create(@RequestParam("userId") final Long userId, @RequestParam("topic") final String topic) {
        try {
            return new ResponseEntity(formService.getFormAnswers(userId, topic), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
