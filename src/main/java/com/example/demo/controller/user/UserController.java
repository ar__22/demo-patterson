package com.example.demo.controller.user;

import com.example.demo.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo-patterson/v1")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/insertUser", method = RequestMethod.PUT)
    public ResponseEntity insertUser(@RequestParam("userName") final String userName) {
        try {
            return new ResponseEntity(userService.insert(userName), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/getUser", method = RequestMethod.GET)
    public ResponseEntity getUser(@RequestParam("userId") final Long userId) {

        try {
            return new ResponseEntity(userService.getUser(userId), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
