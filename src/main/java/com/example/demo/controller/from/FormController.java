package com.example.demo.controller.from;

import com.example.demo.service.form.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/demo-patterson/v1")
public class FormController {

    @Autowired
    private FormService formService;

    @PostMapping("/createForm")
    public ResponseEntity create(@RequestParam("userId") final Long userId,
                                 @RequestParam("topic") final String topic) {
        try {
            return new ResponseEntity(formService.createForm(userId, topic), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/saveAnswer", method = RequestMethod.PUT)
    public ResponseEntity saveAnswer(@RequestParam("formId") final Long formId,
                                     @RequestParam("answer") final String answer,
                                     @RequestParam("topic") final String topic) {
        try {
            return new ResponseEntity(formService.saveAnswer(formId, answer, topic), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/getAnswers", method = RequestMethod.GET)
    public ResponseEntity getAnswers() {
        try {
            return new ResponseEntity(formService.getAnswers(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
