package com.example.demo.service.form;

import com.example.demo.entity.form.FormAnswerEntity;
import com.example.demo.entity.form.FormEntity;
import com.example.demo.entity.form.answerenum.AnswerEnum;

import java.util.List;

public interface FormService {
    FormEntity createForm(final Long userId, final String topic) throws Exception;

    FormAnswerEntity saveAnswer(final Long formId, final String answer, final String topic) throws Exception;

    List<FormAnswerEntity> getFormAnswers(final Long userId, final String topic) throws Exception;

    AnswerEnum[] getAnswers();
}
