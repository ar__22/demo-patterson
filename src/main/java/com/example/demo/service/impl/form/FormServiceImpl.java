package com.example.demo.service.impl.form;

import com.example.demo.entity.form.FormAnswerEntity;
import com.example.demo.entity.form.FormEntity;
import com.example.demo.entity.form.answerenum.AnswerEnum;
import com.example.demo.entity.user.UserEntity;
import com.example.demo.repository.form.FormAnswerRepository;
import com.example.demo.repository.form.FormRepository;
import com.example.demo.repository.user.UserRepository;
import com.example.demo.service.form.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("FormService")
public class FormServiceImpl implements FormService {

    @Autowired
    private FormRepository formRepository;

    @Autowired
    private FormAnswerRepository formAnswerRepository;

    @Autowired
    private UserRepository userRepository;

    final static private String URL = "https://fakeurl.com/";

    @Override
    public FormEntity createForm(final Long userId, final String topic) throws Exception {
        final UserEntity user = userRepository.getOne(userId);
        if (userId.equals(user.getId())) {
            final FormEntity formEntity = buildEntity(user, topic);
            return formRepository.save(formEntity);
        } else {
            throw new Exception("El usuario no existe");
        }
    }

    @Override
    public FormAnswerEntity saveAnswer(final Long formId, final String answer, final String topic) throws Exception {
        final FormAnswerEntity formAnswerEntity = buildEntity(formId, answer, topic);
        return formAnswerRepository.save(formAnswerEntity);
    }

    @Override
    public List<FormAnswerEntity> getFormAnswers(final Long userId, final String topic) throws Exception {

        final UserEntity user = userRepository.getOne(userId);
        if (user != null) {
            final List<FormEntity> formEntities = formRepository.getByUserIdAndTopic(user.getId(), topic);

            if (formEntities != null && formEntities.size() > 0) {
                return formEntities.stream()
                        .map(formEntity -> formAnswerRepository.getByFormId(formEntity.getId()))
                        .collect(Collectors.toList());
            } else {
                throw new Exception("El usuario no tienen ningun formulario asociado");
            }
        } else {
            throw new Exception("El usuario no existe");
        }
    }

    @Override
    public AnswerEnum[] getAnswers() {
        return AnswerEnum.values();
    }


    private FormEntity buildEntity(final UserEntity user, final String topic) {
        final FormEntity formEntity = new FormEntity();
        formEntity.setUrl(URL + user.getUserName().toLowerCase() + "/" + topic.toLowerCase());
        formEntity.setTopic(topic.toUpperCase());
        formEntity.setUserId(user.getId());

        return formEntity;
    }

    private FormAnswerEntity buildEntity(final Long formId, final String answer, final String topic) throws Exception {
        final FormAnswerEntity formAnswerEntity = new FormAnswerEntity();
        formAnswerEntity.setAnswer(validateAnswer(answer));
        formAnswerEntity.setFormId(formId);
        formAnswerEntity.setTopic(topic.toUpperCase());

        return formAnswerEntity;
    }

    private String validateAnswer(final String answer) throws Exception {
        if (AnswerEnum.isMember(answer.toUpperCase())) {
            return answer.toUpperCase();
        } else {
            throw new Exception("La respuesta no es valida");
        }
    }
}
