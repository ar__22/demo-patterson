package com.example.demo.service.impl.user;

import com.example.demo.entity.user.UserEntity;
import com.example.demo.repository.user.UserRepository;
import com.example.demo.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("UserService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public String insert(final String userName) {
        return userRepository.save(buildEntity(userName.toLowerCase())).getUserName();
    }

    @Override
    public Optional<UserEntity> getUser(final Long userId) {
        return userRepository.findById(userId);
    }

    private UserEntity buildEntity(final String userName) {
        final UserEntity userEntity = new UserEntity();
        userEntity.setUserName(userName);

        return userEntity;
    }
}
