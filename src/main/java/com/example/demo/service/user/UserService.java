package com.example.demo.service.user;

import com.example.demo.entity.user.UserEntity;

import java.util.Optional;

public interface UserService {

    String insert(final String userName);

    Optional<UserEntity> getUser(final Long userId);
}
