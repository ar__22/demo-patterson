DROP TABLE IF EXISTS user_table cascade;
DROP TABLE IF EXISTS form_table cascade;
DROP TABLE IF EXISTS form_answer_table cascade;

CREATE TABLE user_table (
    id SERIAL PRIMARY KEY,
    user_name varchar (100) UNIQUE
);

CREATE TABLE form_table (
    id SERIAL PRIMARY KEY,
    url varchar (255),
    topic varchar (50),
    user_id SERIAL REFERENCES user_table(id)
);

CREATE TABLE form_answer_table (
    id SERIAL PRIMARY KEY,
    form_id SERIAL REFERENCES form_table(id) UNIQUE,
    answer varchar (255),
    topic varchar (50)
);

GRANT ALL PRIVILEGES ON DATABASE d9fiqa20mu9r2p TO oabnzniigomxtf;
